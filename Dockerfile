FROM cern/cc7-base:20170809

ADD rpms /rpms/
RUN ls -l
ADD install.sh .
RUN chmod +x install.sh ; ./install.sh
