#!/bin/bash
set -e

sudo systemctl start docker

docker login gitlab-registry.cern.ch

docker build -t gitlab-registry.cern.ch/gcodispo/cc7-l1t-apps .
docker push gitlab-registry.cern.ch/gcodispo/cc7-l1t-apps
