#!/bin/sh

BUILD_RPMS=$1

date > build_date
if [[ $BUILD_RPMS == 'all' ]] ; then
	echo "INFO: Building environment and downloading rpms"
else
	echo "INFO: Building only base environment"
fi

ROOT_DIR=${PWD}

NODEJS_VERSION=6.11.4
NGINX_VERSION=1.12.1
PM2_VERSION=2.4.3
OPENSSL_VERSION=1.0.1e-60.el7.x86_64
SHIB_VERSION=2.6.0

NODEJS_PKG=nodejs$([ -z ${NODEJS_VERSION} ] || echo "-${NODEJS_VERSION}" )
NGINX_PKG=nginx$([ -z ${NGINX_VERSION} ] || echo "-${NGINX_VERSION}" )
OPENSSL_PKG=openssl$([ -z ${OPENSSL_VERSION} ] || echo "-${OPENSSL_VERSION}" )
PM2_PKG=node-pm2$([ -z ${PM2_VERSION} ] || echo "-${PM2_VERSION}" )
SHIB_PKG=pm2$([ -z ${SHIB_VERSION} ] || echo "-${SHIB_VERSION}" )

### define the base cc7 packages
### Warning: over-constrained in order to reproduce June 2017
### Warning: originally requested only:
###          git gcc gcc-c++ make rpm-build ruby ruby-devel
declare -a base_distro=(
		git-1.8.3.1-6.el7_2.1.x86_64
		gcc-4.8.5-11.el7.x86_64
		gcc-c++-4.8.5-11.el7.x86_64
		glibc-2.17-157.el7_3.1.x86_64          #
		glibc-common-2.17-157.el7_3.1.x86_64   #
		glibc-headers-2.17-157.el7_3.1.x86_64  #
		glibc-devel-2.17-157.el7_3.1.x86_64    #
		make-3.82-23.el7.x86_64
		openssh-6.6.1p1-31.el7.x86_64          #
		openssh-server-6.6.1p1-31.el7.x86_64   #
		openssh-clients-6.6.1p1-31.el7.x86_64  #
		rpm-build-4.11.3-21.el7.x86_64
		cpp-4.8.5-11.el7                       #
		libstdc++-4.8.5-11.el7.x86_64          #
		elfutils-0.166-2.el7.x86_64            #
		fipscheck-1.4.1-5.el7.x86_64           #
		ruby-2.0.0.648-29.el7.x86_64
		ruby-devel-2.0.0.648-29.el7
	)

### packages to be downloaded for p5
declare -a dependencies=(
       initscripts-9.49.37-1.el7.x86_64
       iproute-3.10.0-74.el7.x86_64
       iptables-1.4.21-17.el7.x86_64
       libcurl-openssl-7.51.0-2.1.el7.cern.x86_64
       libevent-2.0.21-4.el7.x86_64
       libmemcached-1.0.16-5.el7.x86_64
       libmnl-1.0.3-7.el7.x86_64
       libnetfilter_conntrack-1.0.4-2.el7.x86_64
       libnfnetlink-1.0.1-4.el7.x86_64
       libtool-ltdl-2.4.2-21.el7_2.x86_64
       libxml-security-c17-1.7.3-3.1.el7.cern.x86_64
       sysvinit-tools-2.88-14.dsf.el7.x86_64
       unixODBC-2.3.1-11.el7.x86_64
	)

### explicit dependencies for shibollet
declare -a shib_deps=(
		httpd-2.4.6-2.4.6-45.el7.centos.x86_64
		httpd-tools-2.4.6-2.4.6-45.el7.centos.x86_64
		liblog4shib1-1.0.9-3.1.el7.cern.x86_64
		libxmltooling7-1.6.0-1.1.el7.cern.x86_64
		libxml-security-c17-1.7.3-3.1.el7.cern.x86_64
		libsaml9-2.6.0-1.1.el7.cern.x86_64
	)


######################################################
### installing base dependencies
######################################################

### Install base packages
echo "INFO: Install base packages"
yum install -y yum-plugin-ovl
yum install -y ${base_distro[@]}
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed bulding base distro, exiting";
	exit $ERRCODE
fi

### environement
export PATH=${PATH}:/usr/local/bin
localedef -c -f UTF-8 -i en_US en_US.UTF-8
export LC_ALL=en_US.UTF-8


# log
if [[ $BUILD_RPMS == 'all' ]] ; then
	echo "INFO: Base environment built; downloading rpms"
else
	yum localinstall -y rpms/*/*.rpm
	### installing npm stuff
	export LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib:$LD_LIBRARY_PATH
	gem install fpm
	npm install -g bower abao grunt-cli 'raml2html@3'
	ERRCODE=$?
	if [[ $? != 0 ]]; then
		echo "ERROR: failed npm installation, exiting";
		exit $ERRCODE
	fi

	echo "INFO: Base environment built; exiting."
	exit 0
fi

######################################################
### downloading rpms
######################################################

# dependencies
echo "INFO: Install dependencies"
DEPS_PATH=rpms/deps
mkdir -p ${DEPS_PATH}
yumdownloader --resolve ${dependencies[@]} --destdir=${DEPS_PATH}
yum localinstall -y ${DEPS_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed dependecies installation, exiting";
	exit $ERRCODE
fi


# NodeJS
echo "INFO: Install Node.js"
NODEJS_PATH=rpms/nodejs
mkdir -p ${NODEJS_PATH}
curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
yumdownloader --resolve ${NODEJS_PKG} --destdir=${NODEJS_PATH}
if [[ -z `ls -l ${NODEJS_PATH}/* 2> /dev/null` ]]; then
	echo "ERROR : missing NODEJS version ${NODEJS_VERSION}"
	rm -f ${NODEJS_PATH}*
	exit -1
fi
yum localinstall -y ${NODEJS_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed node.js installation, exiting";
	exit $ERRCODE
fi


test -e nginx.repo || cp setup/nginx/nginx.repo .

# OPENSSL
echo "INFO: Install openssl"
OPENSSL_PATH=rpms/openssl
mkdir -p ${OPENSSL_PATH}
OPENSSL_LIB=openssl-libs$([ -z ${OPENSSL_VERSION} ] || echo "-${OPENSSL_VERSION}" )
OPENSSL_DEV=openssl-devel$([ -z ${OPENSSL_VERSION} ] || echo "-${OPENSSL_VERSION}" )
yumdownloader --resolve -c nginx.repo ${OPENSSL_PKG} ${OPENSSL_LIBS} ${OPENSSL_DEVEL} --destdir=${OPENSSL_PATH}
if [[ -z `ls -l ${OPENSSL_PATH}/* 2> /dev/null` ]]; then
	echo "ERROR : missing OPENSSL version ${OPENSSL_VERSION}"
	rm -f ${OPENSSL_PATH}/*
	exit -1
fi
yum localinstall -y ${OPENSSL_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed openssl installation, exiting";
	exit $ERRCODE
fi

# NGINX
echo "INFO: Install nginx"
NGINX_PATH=rpms/nginx
mkdir -p ${NGINX_PATH}
yumdownloader --resolve -c nginx.repo ${NGINX_PKG} --destdir=${NGINX_PATH}
if [[ -z `ls -l ${NGINX_PATH}/* 2> /dev/null` ]]; then
	echo "ERROR : missing NGINX version ${NGINX_VERSION}"
	rm -f ${NGINX_PATH}/*
	exit -1
fi
yum localinstall -y ${NGINX_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed nginx installation, exiting";
	exit $ERRCODE
fi

# OracleDB Client
echo "INFO: Install Oracle Client"
ORACLE_PATH=rpms/oracledb
mkdir -p ${ORACLE_PATH}
cp setup/database/*.rpm ${ORACLE_PATH}/
yum localinstall -y ${ORACLE_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed Oracle Client installation, exiting";
	exit $ERRCODE
fi

# Shibboleth: only l1ce
echo "INFO: Install Shibollet"
SHIB_PATH=rpms/shibboleth
mkdir -p ${SHIB_PATH}
yumdownloader --resolve shibboleth ${shib_deps[@]} --destdir=${SHIB_PATH}
if [[ -z `ls -l ${SHIB_PATH}/* 2> /dev/null` ]]; then
	echo "ERROR : missing SHIB version ${SHIB_VERSION}"
	rm -f ${SHIB_PATH}*
	exit -1
fi
yum localinstall -y ${SHIB_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed Shibboleth installation, exiting";
	exit $ERRCODE
fi

# FPM {optional, allows you to `make rpm` at P5}
echo "INFO: Install fpm and ruby packages"
gem install fpm
FPM_PATH=rpms/fpm
mkdir -p ${FPM_PATH}
cd ${FPM_PATH}
export PATH=${PATH}:/usr/local/bin
fpm -s gem -t rpm -d "ruby >= 2.0.0" -d rpm-build fpm
fpm -s gem -t rpm -d "ruby >= 2.0.0" hashie      # dep for archive-tar-minitar
fpm -s gem -t rpm -d "ruby >= 2.0.0" powerbar    # dep for archive-tar-minitar
fpm -s gem -t rpm -d "ruby >= 2.0.0" minitar     # dep for archive-tar-minitar
fpm -s gem -t rpm -d "ruby >= 2.0.0" minitar-cli # dep for archive-tar-minitar
fpm -s gem -t rpm -d "ruby >= 2.0.0" archive-tar-minitar
fpm -s gem -t rpm -d "ruby >= 2.0.0" arr-pm
fpm -s gem -t rpm -d "ruby >= 2.0.0" backports
fpm -s gem -t rpm -d "ruby >= 2.0.0" bundler
fpm -s gem -t rpm -d "ruby >= 2.0.0" cabin
fpm -s gem -t rpm -d "ruby >= 2.0.0" childprocess
fpm -s gem -t rpm -d "ruby >= 2.0.0" -v 1.0.1 clamp
fpm -s gem -t rpm -d "ruby >= 2.0.0" ffi
fpm -s gem -t rpm -d "ruby >= 2.0.0" insist
fpm -s gem -t rpm -d "ruby >= 2.0.0" io-like
fpm -s gem -t rpm -d "ruby >= 2.0.0" -v 1.7.7 json
fpm -s gem -t rpm -d "ruby >= 2.0.0" -v 0.99.8 mustache
fpm -s gem -t rpm -d "ruby >= 2.0.0" pleaserun
fpm -s gem -t rpm -d "ruby >= 2.0.0" ruby-xz
fpm -s gem -t rpm -d "ruby >= 2.0.0" stud
fpm -s gem -t rpm -d "ruby >= 2.0.0" dotenv
cd ../..
yum localinstall -y ${FPM_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed fpm installation, exiting";
	exit $ERRCODE
fi

# PM2
echo "INFO: Install pm2"
PM2_PATH=rpms/pm2
PM2_VERSION_STR=$([ ${PM2_VERSION} ] && echo "-v " )${PM2_VERSION}
mkdir -p ${PM2_PATH}
cd ${PM2_PATH}
fpm -s npm -t rpm ${PM2_VERSION_STR} pm2
cd ../..
if [[ -z `ls -l ${PM2_PATH}/* 2> /dev/null` ]]; then
	echo "ERROR : missing PM2 version ${PM2_VERSION}"
	rm -f ${PM2_PATH}*
	exit -1
fi
yum localinstall -y ${PM2_PATH}/*.rpm
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed pm2 installation, exiting";
	exit $ERRCODE
fi


export LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib:$LD_LIBRARY_PATH

npm install -g bower abao grunt-cli 'raml2html@3'
ERRCODE=$?
if [[ $? != 0 ]]; then
	echo "ERROR: failed npm installation, exiting";
	exit $ERRCODE
fi

ls -l rpms/*/*

